import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
		boolean check = true;
		int guess; 
		int count=0;
		do{
			System.out.println("Type -1 to quit or guess a number 0-99:");
			guess= reader.nextInt();
			count++;
			if(guess == number)
				System.out.println("Congratulations! You won after " + count + " attemps");
			else if(guess == -1)
				System.out.println("Sorry, the number is" + number);
			else{
				if(guess<number)
					System.out.println("Mine ise greater than your guess");
				else if (guess>number)
					System.out.println("Mine ise less than your guess");
		}
		}while((guess!=number) && (guess!=-1));
	}


}

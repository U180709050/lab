public class FindLetterGrade{

	public static void main(String[] args){
	int value=Integer.parseInt(args[0]);
	if (value<0 || value>100){
		System.out.println("It is not a valid score");
	}else if (value<60){
		System.out.println("Your grade is F");
	}else if (value<70){
		System.out.println("Your grade is D");
	}else if (value<80){
		System.out.println("Your grade is C");
	}else if (value<90){
		System.out.println("Your grade is B");
	}else if (value<100){
		System.out.println("Your grade is A");
	}
	}
}

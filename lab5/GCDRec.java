public class GCDRec{

	public static void main(String[] args){

		int num1 = Integer.parseInt(args[0]);
		int num2 = Integer.parseInt(args[1]);
		int gcd = gcd(num1,num2);

		System.out.println(gcd);
	}

	public static int gcd(int num1, int num2){
		if(num2!= 0)
			return gcd(num2,num1 % num2);
		else
			return num1;
	}
}

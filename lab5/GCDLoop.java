public class  GCDLoop{
	public static void main(String[] args){
		int num1 = Integer.parseInt(args[0]);
		int num2 = Integer.parseInt(args[1]);
		int gcd = gcd(num1,num2);
		System.out.println(gcd);
	}

	public static int gcd(int num1, int num2){
		int r = 0;
		while(num2!= 0){
			r = num1 % num2;
			num1 = num2;
			num2 = r;
	}
		return num1;
	}
}



public class Main {

	public static void main(String[] args) {
		Point p = new Point(4,6);
		Rectangle r = new Rectangle(5,9,p);
		System.out.println(r.area());
		System.out.println(r.perimeter());
		Point[] points = r.corners();
		for(int i = 0; i < 4; i++) {
			System.out.print(points[i].xCoord+" ");
			System.out.println(points[i].yCoord);
		}
		Point point2 = new Point (2,3);
		Circle circle1 = new Circle(10,point2);
		Circle circle2 = new Circle(4,p);
		System.out.println(circle1.area());
		System.out.println(circle1.perimeter());
		System.out.println(circle1.intersect(circle2));
	}

}

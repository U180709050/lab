
public class Rectangle {
	int sideA;
	int sideB;
	Point topleft = new Point(sideA,sideB);
	
	public Rectangle(int a, int b, Point point) {
		this.sideA = a;
		this.sideB = b;
		this.topleft = point;
	}
	
	public int area() {
		return(this.sideA*this.sideB);
	}
	
	public int perimeter() {
		return((this.sideA+this.sideB)*2);
	}
	
	public Point[] corners() {
		Point[] points = new Point[4];
		points[0] = topleft;
		points[1] = new Point(topleft.xCoord + sideA,topleft.yCoord);
		points[2] = new Point(topleft.xCoord,topleft.yCoord - sideB);
		points[3] = new Point(topleft.xCoord + sideA ,topleft.yCoord - sideB);
		return points;
		
		}
		
	}


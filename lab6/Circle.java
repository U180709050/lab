
public class Circle {
	int radius;
	Point center;
	
	public Circle(int r, Point c) {
		this.radius = r;
		this.center = c;
	}
	
	public double area() {
		return Math.PI*(Math.pow(this.radius, 2));
	}
	
	public double perimeter() {
		return Math.PI*this.radius*2;
	}
	
	public boolean intersect(Circle a) {
		double distance = Math.sqrt(Math.pow((this.center.xCoord - a.center.xCoord), 2) + Math.pow((this.center.yCoord - a.center.yCoord), 2));
		double sumRad = this.radius + a.radius;
		if (distance < sumRad) {
			return true;
		}
		else
			return false;
	}
}
